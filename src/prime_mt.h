/******************************************************************************
 * Technische Informatik 2                                                    *
 *                                                                            *
 * Exercise 7 - Task 3                                                        *
 *                                                                            *
 * Student:                  Felix Rüdiger                                    *
 * Matrikel No.:             214002                                           *
 *                                                                            *
 * File:                     prime_mt.h                                       *
 *                                                                            *
 * Original source location: https://gitlab.com/fruediger-ti2/prime_mt        *
 *                                                                            *
 ******************************************************************************/

#ifndef PRIME_MT_H__
#define PRIME_MT_H__

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <libgen.h>
#include <getopt.h>
    
#define VERSION_MAJOR (0)
#define VERSION_MINOR (1)
#define VERSION_PATCH (0)

#define DEFAULT_START_NUMBER (10000000000000000LL)
#define DEFAULT_END_NUMBER   (-1LL)
#define DEFAULT_STEP_SIZE    (1LL)
#define DEFAULT_MIN_THREADS  (2)
#define DEFAULT_MAX_THREADS  (16)
#define MINIMUM_VERBOSITY    (0)

long long int start      = DEFAULT_START_NUMBER;
long long int end        = DEFAULT_END_NUMBER;
long long int step       = DEFAULT_STEP_SIZE;         
long          numThreads = DEFAULT_MIN_THREADS;
int           verbosity  = MINIMUM_VERBOSITY;

#define log(level, msg, ...) if (level <= verbosity) printf(msg "\n", ##__VA_ARGS__)

void init(int argc, char *argv[]);

static const struct option options[] =
{
    { "from",        required_argument, NULL, 'f' },
    { "to",          required_argument, NULL, 't' },
    { "step",        required_argument, NULL, 's' },
    { "min-threads", required_argument, NULL, 'l' },
    { "max-threads", required_argument, NULL, 'u' },
    { "num-threads", required_argument, NULL, 'n' },
    { "verbose",     optional_argument, NULL, 'v' },
    { "version",     no_argument,       NULL, 'V' },
    { "help",        no_argument,       NULL, 'h' }
};

static void version()
{
    printf("%i.%i.%i\n", VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH);
}

static void usage(const char *executableName, long numCPU)
{
    printf("USAGE: %s [OPTION...]\n", executableName);
    printf("\n");
    printf("OPTIONs are:\n");
    printf("\n");
    printf("    -f=, --from=           Specifies the first number to check for if it's a\n");
    printf("                           prime number or not.\n");
    printf("                           If the given value is a zero or negative number,\n");
    printf("                           %lld is used instead.\n", DEFAULT_START_NUMBER);
    printf("                           Defaults to %lld.\n", DEFAULT_START_NUMBER);
    printf("\n");
    printf("    -t=, --to=             Specifies the (including) last number to check for\n");
    printf("                           if it's a prime number or not.\n");
    printf("                           Positive numbers between the first number and the\n");
    printf("                           last number to check (with respect to the step size;\n");
    printf("                           see below) are tested for there prime number pro-\n");
    printf("                           perty. The application stops when the given last\n");
    printf("                           number was overstepped (respectively understepped;\n");
    printf("                           depending on the relation between the first number\n");
    printf("                           and the last number).\n");
    printf("                           Defaults to %lld.\n", DEFAULT_END_NUMBER);
    printf("\n");
    printf("    -s=, --step=           Specifies the step size taken from one number to\n");
    printf("                           check for to the next number.\n");
    printf("                           If the given value is zero, %lld is\n", DEFAULT_STEP_SIZE);
    printf("                           used instead.\n");
    printf("                           Defaults to %lld.\n", DEFAULT_STEP_SIZE);
    printf("\n");
    printf("    -l=, --min-threads=    Specifies the minimum number of parallel worker\n");
    printf("                           threads to determine if a number is a prime number or\n");
    printf("                           not.\n");
    printf("                           This number is used, if no specific number of threads\n");
    printf("                           is requested (via the -n or --num-threads options)\n");
    printf("                           and the number of logical processors available to\n");
    printf("                           this application is LESS than the given minimum\n");
    printf("                           number.\n");
    printf("                           Use 0 (or any negative value) to disable this mecha-\n");
    printf("                           nism.\n");
    printf("                           Defaults to %d.\n", DEFAULT_MIN_THREADS);
    printf("\n");
    printf("    -u=, --max-threads=    Specifies the maximum number of parallel worker\n");
    printf("                           threads to determine if a number is a prime number or\n");
    printf("                           not.\n");
    printf("                           This number is used, if no specific number of threads\n");
    printf("                           is requested (via the -n or --num-threads options)\n");
    printf("                           and the number of logical processors available to\n");
    printf("                           this application is GREATER than the given minimum\n");
    printf("                           number.\n");
    printf("                           Use 0 (or any negative value) to disable this mecha-\n");
    printf("                           nism.\n");
    printf("                           The maximum mechanism TAKES PRIORITY over specifying\n");
    printf("                           a minimum number of threads!\n");
    printf("                           Defaults to %d.\n", DEFAULT_MAX_THREADS);
    printf("\n");
    printf("    -n=, --num-threads=    Specifies the exact number of parallel worker threads\n");
    printf("                           to determine if a number is a prime number or not.\n");
    printf("                           This number is used, regardless to the values set (or\n");
    printf("                           may not set) to minimum or maximum number of threads\n");
    printf("                           (via the -l, --min-threads, -u, or --max-threads opt-\n");
    printf("                           ions) and regardless to number of logical processors\n");
    printf("                           available to this application.\n");
    printf("                           If no value is given or if a zero or negative value\n");
    printf("                           is given, the number of parallel worker threads is\n");
    printf("                           determined by the number of logical processors avail-\n");
    printf("                           able to this application with regard to the minimum/\n");
    printf("                           maximum mechanism described above.\n");
    printf("                           The number of logical processors is %ld.\n", numCPU);
    printf("\n");
    printf("    -v[=],--verbose[=]     Increases the verbosity level (you can specify this\n");
    printf("                           option multiple times in order to increase the\n");
    printf("                           verbosity to a certain level) or sets it to the opt-\n");
    printf("                           ional given (non-negative) value.\n");
    printf("\n");
    printf("    --version              Prints version information about this application\n");
    printf("                           then exits.\n");
    printf("\n");
    printf("    -h, --help             Prints this information then exits.\n");
}

void init(int argc, char *argv[])
{
    long numCPU      = sysconf(_SC_NPROCESSORS_ONLN),
         minThreads  = DEFAULT_MIN_THREADS,
         maxThreads  = DEFAULT_MAX_THREADS;
    int  showVersion = 0,
         showHelp    = 0;

    numThreads = 0;

    int optidx = 0, opt;
    while ((opt = getopt_long(argc, argv, "f:t:s:l:u:n:v::h", options, &optidx)) != -1)
        switch (opt)
        {
            case 'f':
                start = atoll(optarg);
                break;

            case 't':
                end = atoll(optarg);
                break;

            case 's':
                step = atoll(optarg);
                break;

            case 'l':
                minThreads = atol(optarg);
                break;

            case 'u':
                maxThreads = atol(optarg);
                break;

            case 'n':
                numThreads = atol(optarg);
                break;

            case 'v':
                if (optarg)
                {
                    if ((verbosity = atoi(optarg)) < MINIMUM_VERBOSITY)
                        verbosity = MINIMUM_VERBOSITY;
                }
                else
                    verbosity++;
                break;

            case 'V':
                showVersion = 1;
                break;

            case 'h':
                showHelp = 1;
                break;

            case ':':
            case '?':
            default:
                printf("Try this instead:\n");
                usage(basename(argv[0]), numCPU);
                exit(EXIT_FAILURE);
                break;
        }

    if (showHelp)
    {
        usage(basename(argv[0]), numCPU);
        exit(EXIT_SUCCESS);
    }

    if (showVersion)
    {
        version();
        exit(EXIT_SUCCESS);
    }

    if (start <= 0LL)
        start = DEFAULT_START_NUMBER;

    if (step == 0LL)
        step = DEFAULT_STEP_SIZE;

    if (numThreads <= 0)
    {
        numThreads = numCPU;

        if ((minThreads > 0) && (numThreads < minThreads))
            numThreads = minThreads;

        if ((maxThreads > 0) && (numThreads > maxThreads))
            numThreads = maxThreads;
    }
}

#endif