/******************************************************************************
 * Technische Informatik 2                                                    *
 *                                                                            *
 * Exercise 7 - Task 3                                                        *
 *                                                                            *
 * Student:                  Felix Rüdiger                                    *
 * Matrikel No.:             214002                                           *
 *                                                                            *
 * File:                     prime_mt.c                                       *
 *                                                                            *
 * Original source location: https://gitlab.com/fruediger-ti2/prime_mt        *
 *                                                                            *
 ******************************************************************************/

#include <stdlib.h>
#include <stdbool.h>
#include <errno.h>
#include <pthread.h>
#include "prime_mt.h"

typedef struct
{
    long            tid;     // thread id
    pthread_t       pthread; // pthread structure representing this thread
    long long int   arg;     // the prime candidate to check as input argument
    volatile int   *signal;  // a common signal slot in shared memory, to signal a found non-prime number
} primeThread_t;

void *primeThread(void *th)
{
    primeThread_t thread  = *((primeThread_t *)th);
    long long int divisor = thread.tid + 3;

    for (; !*(thread.signal) && (divisor * divisor) < thread.arg; divisor += numThreads)
    {
        log(3, "[%ld]: Checking if %lld is a factor of %lld.", thread.tid, divisor, thread.arg);

        if (thread.arg % divisor == 0)
        {
            log(2, "[%ld]: Success! %lld is a factor of %lld. Therefore %lld cannot be a prime number.", thread.tid, divisor, thread.arg, thread.arg);

            *(thread.signal) = 1;
            pthread_exit(NULL);
        }
    }

    log(3, "[%ld]: Could not found a factor of %lld. Maybe it's a prime number?", thread.tid, thread.arg);

    pthread_exit(NULL);
}

bool prime(long long int p, primeThread_t *threads)
{
    log(2, "Checking %lld for prime property.", p);

    if(p==2 || p==3) 
    {
        log(2, "%lld is a prime number.", p);
		return true;        
    }
	if(p<2 || (p%2)==0 || ((p%4)!=1 && (p%4)!=3) || ((p%6)!=1 && (p%6)!=5))
    {
        log(2, "%lld for sure is not a prime number.", p);
		return false;
    }

    log(3, "Creating %ld threads to check for prime property on %lld.", numThreads, p);

    // resetting the signal slot
    *(threads[0].signal) = 0;

    long tid;

    // creating new threads
    for (tid = 0; tid < numThreads; tid++)
    {
        threads[tid].arg = p;
        if (pthread_create(
                &(threads[tid].pthread), // pthread structure
                NULL,                    // pthread attributes
                &primeThread,            // thread function
                &threads[tid]            // argument passed to the thread function (here our thread structure)
            ) != 0)
        {
            perror("One or more threads could not be created");
            exit(EXIT_FAILURE);
        }
    }

    // waiting for them to complete
    for (tid = 0; tid < numThreads; tid++)
    {
        if (pthread_join(threads[tid].pthread, NULL) != 0)
        {
            perror("One or more threads could not be joined");
            exit(EXIT_FAILURE);
        }
    }

    if (*threads[0].signal)
    {
        log(2, "%lld for sure is not a prime number.", p);
        return false;
    }
    else
    {
        log(2, "%lld is a prime number.", p);
        return true;
    }
}

int main(int argc, char *argv[])
{
    init(argc, argv);

    log(4, "Verbosity is %d.", verbosity);
    log(3, "Using up to %ld threads.", numThreads);
    log(3, "Checking numbers from %lld to %lld (each time incremented by %lld).", start, end, step);
    log(1, "Use CTRL+C to quit.");

    primeThread_t *threads     = (primeThread_t *)malloc(numThreads * sizeof(primeThread_t));
    volatile int   signalSlot = 0;
    for (long tid = 0; tid < numThreads; tid++)
    {
        threads[tid].tid    = tid;
        threads[tid].signal = &signalSlot;
    }

    long long int n = start,
                  k = end - start;
    if (k < 0)
    {
        for (k = -k; n > 0 && k >= 0; n += step, k += step)
            if (prime(n, threads))
                log(0, "%lld", n);
    }
    else
    {
        for (; n > 0 && k >= 0; n += step, k -= step)
            if (prime(n, threads))
                log(0, "%lld", n);
    }

    free(threads);
}